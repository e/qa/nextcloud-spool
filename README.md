# NextCloud spool

git-lfs project to share testing spool about NextCloud.

## Useful command

### Replicate a given file n time

```bash
f=spool/Pictures/Screenshot.png; for x in {1..2000}; do cp $f $(printf "${f%.*}_%04d.${f##*.}\n" $x); done
```

The command below will replicate the `Screenshot.png` files 2000 times with a filename like that:

```
spool/Pictures/                                            
├── Screenshot_0001.png                                    
├── Screenshot_0002.png                                    
├── Screenshot_0003.png                                    
├── Screenshot_0004.png                                    
├── Screenshot_0005.png                                    
├── ...
```

